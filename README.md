# Moodle Quiz Generator

Python program that generate set of randomized Moodle Quiz questions.

## Getting started

At the moment, program description is mainly written inside the file [*generate_new_quiz.ipynb*](https://gitlab.com/aleksander_grm/moodle-quiz-generator/-/blob/main/generate_new_quiz.ipynb)! But in the future will be also here 😉.
